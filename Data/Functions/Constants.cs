﻿//Nikolay Chernuha ID: 323434803
//Eyal Ilan ID:304929334
//Naor Zino ID: 200804367

namespace Data.Functions
{
    //Vonstants that are used by LZSS algorithhm
    public static class Constants
    {
        public const int WindowSize = 4096;

        public const int NullIndex = WindowSize + 1;

        public const int MaxUncoded = 2;

        public const int MaxCoded = MaxUncoded + 16;

        public const int HashSize = 1024;
    }
}
