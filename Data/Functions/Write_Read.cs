﻿//Nikolay Chernuha ID: 323434803
//Eyal Ilan ID:304929334
//Naor Zino ID: 200804367

using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace Data.Functions
{
    //Compress and Decompress functions with passed optionf of compression, 
    //very straight forward, very simple. Those functions read and write files.

    public class Write_Read
    {
        public static void Compress(string path, bool? LZSS, bool? haffman)
        {
            byte[] outp = null;
            mycomp output = null;
            if (LZSS == true && haffman == true)
            {
                var MiddleArray = Lzss.Encode(path);
                outp = HufmannEncoderDecoder.CompressWithArray(MiddleArray, "");
                output = new mycomp(outp, true, true, System.IO.Path.GetFileName(path).Split('.').Last());
            }
            else if (LZSS == true && haffman == false)
            {
                outp = Lzss.Encode(path);
                output = new mycomp(outp, true, false, System.IO.Path.GetFileName(path).Split('.').Last());
            }
            else if (LZSS == false && haffman == true)
            {
                outp = HufmannEncoderDecoder.CompressWithArray(null, path);
                output = new mycomp(outp, false, true, System.IO.Path.GetFileName(path).Split('.').Last());
            }
            try
            {
                string ret = System.IO.Path.GetDirectoryName(path) + "\\" + System.IO.Path.GetFileName(path).Split('.').First() + ".mycomp";
                Stream stream = File.Open(ret, FileMode.Create);
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(stream, output);
                stream.Close();
            }
            catch { }
        }
        public static void DeCompress(string Path)
        {
            Stream stream = null;
            try
            {
                stream = File.Open(Path, FileMode.Open);
            }
            catch { }
            BinaryFormatter bf = new BinaryFormatter();
            mycomp forDecomp = null;
            try
            {
                forDecomp = (mycomp)bf.Deserialize(stream);
            }
            catch { return; }
            byte[] data = null;
            if (forDecomp._LZSS == true && forDecomp._Huff == true)
            {
                data = Lzss.Decode(HufmannEncoderDecoder.DecompressWithArray(forDecomp._Arr));
            }
            else if (forDecomp._LZSS == true && forDecomp._Huff == false)
            {
                data = Lzss.Decode(forDecomp._Arr);
            }
            else if (forDecomp._LZSS == false && forDecomp._Huff == true)
            {
                data = HufmannEncoderDecoder.DecompressWithArray(forDecomp._Arr);
            }
            try
            {
                File.WriteAllBytes(System.IO.Path.GetDirectoryName(Path) + "\\"+System.IO.Path.GetFileName(Path).Split('.').First() + "Decomp." + forDecomp._Ending, data);
                
            }
            catch { }
        }
    }
}
