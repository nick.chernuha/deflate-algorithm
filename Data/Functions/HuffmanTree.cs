﻿//Nikolay Chernuha ID: 323434803
//Eyal Ilan ID:304929334
//Naor Zino ID: 200804367

using System.Collections.Generic;

namespace Data.Functions
{
    public class HuffmanTree
    {
        /*The function which builds the HashMap of bytes and codes*/
        public static Dictionary<byte, string> HaffmanCodeMap(HuffmanNode node, string code, Dictionary<byte, string> map)
        {
            if (node == null)
                return null;
            if (node.Letter != null)
                map.Add((byte)node.Letter, code);
            HaffmanCodeMap(node.left, code + "1", map);
            HaffmanCodeMap(node.right, code + "0", map);
            return map;
        }
    }
}
