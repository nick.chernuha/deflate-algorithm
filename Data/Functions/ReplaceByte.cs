﻿//Nikolay Chernuha ID: 323434803
//Eyal Ilan ID:304929334
//Naor Zino ID: 200804367

namespace Data.Functions
{
    public static class ReplaceByte
    {
        //Replaces the characters at a cpicific index
        public static void ReplaceChar(int ByteIndex, byte NewByte)
        {
            var firstIndex = ByteIndex - Constants.MaxUncoded - 1;
            if (firstIndex < 0)
            {
                firstIndex += Constants.WindowSize;
            }

            // Remove all hash entries containing character at ByteIndex
            for (var i = 0; i < Constants.MaxUncoded + 1; i++)
            {
                RemoveStr.RemoveString((firstIndex + i) % Constants.WindowSize);
            }

            Lzss.SlidingWindow[ByteIndex] = NewByte;

            for (var i = 0; i < Constants.MaxUncoded + 1; i++)
            {
                AddSeq.AddSequence((firstIndex + i) % Constants.WindowSize);
            }
        }

    }
}
