﻿//Nikolay Chernuha ID: 323434803
//Eyal Ilan ID:304929334
//Naor Zino ID: 200804367

namespace Data.Functions
{
    //This function adds sequence to the the upconming array
    public static class AddSeq
    {
        public static void AddSequence(int ByteIndex)
        {
            // Inserted character will be at the end of the list
            Lzss.Next[ByteIndex] = Functions.Constants.NullIndex;

            var hashKey = GetHashKey.GetKey(ByteIndex, false);

            // This is the only character in the list
            if (Lzss.EncodedTable[hashKey] == Functions.Constants.NullIndex)
            {
                Lzss.EncodedTable[hashKey] = ByteIndex;
                return;
            }

            // Find the end of the list
            var i = Lzss.EncodedTable[hashKey];
            while (Lzss.Next[i] != Functions.Constants.NullIndex)
            {
                i = Lzss.Next[i];
            }

            // Add new character to the list end
            Lzss.Next[i] = ByteIndex;
        }

    }
}
