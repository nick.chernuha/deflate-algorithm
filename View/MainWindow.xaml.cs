﻿//Nikolay Chernuha ID: 323434803
//Eyal Ilan ID:304929334
//Naor Zino ID: 20804367

using System;
using System.Windows;
using System.ComponentModel;

namespace View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //static fileds
        public static string FileName = "";
        public string FileNamed = "";
        public static bool canCompress = false;
        public MainWindow()
        {
            InitializeComponent();

        }

        private void CompressBtn_Click(object sender, RoutedEventArgs e)
        {
            //Background worker - new thread to avoid UI freeze,
            //it dies after the task finishs and the GC cleans it automatically
            BackgroundWorker bg = new BackgroundWorker();
            bg.DoWork += CompressWork;
            bg.RunWorkerAsync();
        }

        private void CompressWork(object sender, DoWorkEventArgs e)
        {
            bool? _lzss = null, _haf = null;
            bool RunOnce = false;
            //Can't access directly to the UI thread, have to do it thru dispatcher
            while (RunOnce == false)
            {
                this.Dispatcher.BeginInvoke((Action)delegate ()
                {
                    Spinner.Visibility = Visibility.Visible;
                    CompressBtn.IsEnabled = false;
                    browsCompBtn.IsEnabled = false;
                    lzssCheck.IsEnabled = false;
                    hafCheck.IsEnabled = false;
                    DecompTab.IsEnabled = false;
                    _lzss = lzssCheck.IsChecked;
                    _haf = hafCheck.IsChecked;
                    ProcessingLbl.Visibility = Visibility.Visible;
                    canCompress = (lzssCheck.IsChecked == true || hafCheck.IsChecked == true);
                    RunOnce = true;
                });
               
            }
            if (canCompress)
            {
                if (FileName != "")
                {
                    //Compressing the staff
                    Data.Functions.Write_Read.Compress(FileName, _lzss, _haf);
                }
                else
                {
                    MessageBox.Show("Chose a file to compress first!");
                }
            }
            //else alert window - nothing been chosen.
            else
            {
                if (FileName != "")
                {
                    MessageBox.Show("Chose a compress option first.");
                }
                else
                {
                    MessageBox.Show("Chose a file to compress first!");
                }
            }
            this.Dispatcher.BeginInvoke((Action)delegate ()
            {
                Spinner.Visibility = Visibility.Hidden;
                CompressBtn.IsEnabled = true;
                browsCompBtn.IsEnabled = true;
                lzssCheck.IsEnabled = true;
                hafCheck.IsEnabled = true;
                DecompTab.IsEnabled = true;
                ProcessingLbl.Visibility = Visibility.Hidden;
            });
        }
        //Decompress Button
        private void DeCompressBtn_Click(object sender, RoutedEventArgs e)
        {
            //Same as compress
            BackgroundWorker bg = new BackgroundWorker();
            bg.DoWork += DecompressWork;
            bg.RunWorkerAsync();

        }
        //Same as Compress
        private void DecompressWork(object sender, DoWorkEventArgs e)
        {
            this.Dispatcher.BeginInvoke((Action)delegate ()
            {
                Spinner.Visibility = Visibility.Visible;
                DeCompressBtn.IsEnabled = false;
                browsDeCompBtn.IsEnabled = false;
                CompTab.IsEnabled = false;
                ProcessingLbl.Visibility = Visibility.Visible;
            });
            if (FileNamed != "")
            {
                Data.Functions.Write_Read.DeCompress(FileNamed);
            }
            else
            {
                MessageBox.Show("Chose a file to decompress first!");
            }
            this.Dispatcher.BeginInvoke((Action)delegate ()
            {
                Spinner.Visibility = Visibility.Hidden;
                ProcessingLbl.Visibility = Visibility.Hidden;
                DeCompressBtn.IsEnabled = true;
                browsDeCompBtn.IsEnabled = true;
                CompTab.IsEnabled = true;
            });
        }
        //FAQ button
        private void FAQ_Click(object sender, RoutedEventArgs e)
        {
            FAQWindow w = new FAQWindow();
            w.Show();
        }

        //Compress Browse Button, no file filter
        private void browsCompBtn_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            // Set filter for file extension and default file extension
            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();
            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                // Open document
                FileName = dlg.FileName;
                CompressChoosenPath.Text = FileName;
            }
        }

        //Decompress Browse Button - file filter *.mycomp
        private void browsDeCompBtn_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".mycomp";
            dlg.Filter = "Text documents (.mycomp)|*.mycomp";
            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();
            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                // Open document
                FileNamed = dlg.FileName;
                DeCompressChoosenPath.Text = FileNamed;
            }
        }
    }
}
