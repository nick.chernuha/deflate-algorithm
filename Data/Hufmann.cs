﻿//Nikolay Chernuha ID: 323434803
//Eyal Ilan ID:304929334
//Naor Zino ID: 200804367

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using VCSKicksCollection;

namespace Data
{
    //Huffman Encoder/Decoder class
    public static class HufmannEncoderDecoder
    {
        public static BitArray temp = null;
        //Encoder Function - The same as from homework, just adapted for C#
        public static byte[] CompressWithArray(byte[] input, string inputFileNameForEncode)
        {
            
            //Because of the given option of separate encoding, 
            //i'm checking if the inputFileNamu is not an empty string, 
            //if it is- then ihis is a second stage decoding.
            if (inputFileNameForEncode != "")
            {
                try
                {
                    input = File.ReadAllBytes(inputFileNameForEncode);
                }
                catch { }
                if (input == null)
                {
                    return null;
                }
            }



            /*Butilding a dictionary (hashmap) with key as Byte and integer to count the number of a kind*/
            Dictionary<byte, int> CharacterMap = new Dictionary<byte, int>();
            foreach (byte val in input)
            {
                if (!CharacterMap.ContainsKey(val))
                {
                    CharacterMap.Add(val, 1);
                }
                else
                {
                    CharacterMap[val] += 1;
                }
            }
            /*Filling the queue to later make it as tree*/
            PriorityQueue<HuffmanNode> myQueue = new PriorityQueue<HuffmanNode>();
            foreach (KeyValuePair<byte, int> current in CharacterMap)
            {
                myQueue.Enqueue(new HuffmanNode(current.Key, current.Value));
            }
            while (myQueue.Count > 1)
            {
                HuffmanNode node1 = myQueue.Dequeue();
                HuffmanNode node2 = myQueue.Dequeue();
                HuffmanNode parent = new HuffmanNode(null, node1.Freq + node2.Freq); // null - means not a leaf
                parent.left = node1;
                parent.right = node2;
                myQueue.Enqueue(parent);
            }

            //Map of Byte as key and String as Hufmann code
            Dictionary<byte, string> CodeMap = Functions.HuffmanTree.HaffmanCodeMap(myQueue.Dequeue(), "", new Dictionary<byte, string>());
            int sizeOfBitSet = 0;
            StringBuilder codes = new StringBuilder();
            //iterating thru the map to build a string of keys
            foreach (KeyValuePair<byte, string> leaf in CodeMap)
            {
                codes.Append(leaf.Key + "==" + leaf.Value + "  ");
                sizeOfBitSet += (CharacterMap[leaf.Key] * (leaf.Value).Length); //calculating the number of bist i need == number of codesInBytes of a kind times code length for each of each
            }
            BitArray codesInBits = new BitArray(Encoding.ASCII.GetBytes(codes.ToString()));
            sizeOfBitSet += Encoding.UTF8.GetBytes(codes.ToString()).Length*8;
            sizeOfBitSet += 16; //adding length of a key - int=16bit
            string sizeInBinary = Convert.ToString(codes.Length, 2);
            string s2 = Functions.Converters.BitArrayToStr(codesInBits);

            //if the binary representation of the key length is less the 16 bit, 
            //i'm adding zeros to the beginning to match the pattern of 16 bits.
            while (sizeInBinary.Length < 16)
                sizeInBinary = "0" + sizeInBinary;

            int iterator = 0;

            /*adding the key to bitset byte per byte*/
            List<bool> before = new List<bool>();
            BitArray beforeA = new BitArray(sizeOfBitSet);
            int ii = 0;
            for (int i = 0; i < sizeInBinary.Length; i++)
            {
                if (sizeInBinary[i] == '1')
                {
                    beforeA[ii] = true;
                    ii++;
                    //before.Add(true);
                    iterator += 1;
                }
                else
                {
                    beforeA[ii] = false;
                    ii++;
                    //before.Add(false);
                    iterator += 1;
                }
            }
            temp = codesInBits;
            /*adding the key itself to the outbitset*/
            for (int i = 0; i < codesInBits.Length; i++)
            {
                if (codesInBits[i])
                {
                    beforeA[ii] = true;
                    ii++;
                }
                   // before.Add(true);
                if (!codesInBits[i])
                {
                    beforeA[ii] = false;
                    ii++;
                }
                //before.Add(false);
            }

            /*adding the file as new bits*/
            for (int i = 0; i < input.Length; i++)
            {
                for (int j = 0; j < CodeMap[input[i]].Length; j++)
                {
                    if (CodeMap[input[i]][j] == '1')
                    {
                        beforeA[ii] = true;
                        ii++;
                    }
                   // before.Add(true);
                    else
                    {
                        beforeA[ii] = false;
                        ii++;
                    }
                    //before.Add(false);
                }
            }
            return Functions.ConverterTo.ToByteArray(beforeA);
            BitArray outBitSet = new BitArray(before.Count);
            for (int i = 0; i < before.Count; i++)
            {
                outBitSet[i] = before[i];
            }

            //returning the byte array
            return Functions.ConverterTo.ToByteArray(outBitSet);
        }


        public static byte[] DecompressWithArray(byte[] input)
        {
            byte[] data = input;
            bool[] reverse = new bool[input.Length*8];
            List<bool> revTmp = new List<bool>();
            int reviter = 0;
            //Reversing the sequence, not very efficient, however it really simplifies the work.
            //The proble here is that each BitArray is reversed and the reading from it wrong
            //entered data to BitArray -> ABCDEFGH, reading the data -> HGFEDCBA
            //Thats okay because of the bit storage inside every byte, however the built in
            //reading method of BitArray does not support "correct" reading as Java's BitSet
            BitArray dataInBits = new BitArray(input);
            int iter = 0,tIter=0;
            for (int i = 0; i < dataInBits.Count; i++)
            {
                if (reviter == 8)
                {
                    revTmp.Reverse();
                    for(int j = 0; j < 8; j++)
                    {
                        dataInBits[iter] = revTmp[j];
                        iter += 1;

                    }
                    //reverse.AddRange(revTmp);
                    revTmp.Clear();
                    revTmp.Add(dataInBits[i]);
                    reviter = 1;
                }
                else
                {
                    revTmp.Add(dataInBits[i]);
                    reviter += 1;
                }
            }
            StringBuilder lengthOfKey = new StringBuilder();
            revTmp.Reverse();
            for (int j = 0; j < revTmp.Count; j++)
            {
                dataInBits[iter] = revTmp[j];
                iter += 1;

            }
            //reverse.AddRange(revTmp);
            for (int i = 0; i < 16; i++)
            {
                if (dataInBits[i])
                    lengthOfKey.Append(1);
                else
                    lengthOfKey.Append(0);
            }

            string keys = null;
            int numofbytes = Convert.ToInt32(lengthOfKey.ToString(), 2);// Integer.parseInt(lengthOfKey.ToString(), 2);

            int segIter = 0, stringIter = 0;
            
            byte[] byteString = new byte[numofbytes + 1];
            bool[] codes = new bool[numofbytes * 8];
            BitArray temp = new BitArray(8);
            for (int i = 16; i < numofbytes * 8 + 16; i++)
            {
                codes[i - 16] = dataInBits[i];
            }
            for (int i = 0; i < codes.Length; i++)
            {
                if (segIter == 8)
                {
                    byteString[stringIter] =  Functions.Converters.ConvertToByte(temp);
                    segIter = 0;
                    stringIter += 1;
                    temp[segIter] = codes[i];
                    segIter += 1;
                }
                else
                {
                    temp[segIter] = codes[i];
                    segIter += 1;
                }
            }
            //The conversion is in try because the conversion can always go wrong,
            //evey if something goes wrong, the program would not vrash
            try
            {
                keys = Encoding.ASCII.GetString(byteString, 0, byteString.Length);
            }
            catch
            { }
            //The dictionar of string sequence
            Dictionary<string, byte> EncodingMap = new Dictionary<string, byte>();
            string[] stringSeparators = new string[] { "  " };
            string[] stringSeparatorsEq = new string[] { "==" };
            string[] SplitKeys = keys.Split(stringSeparators, StringSplitOptions.None);
            StringBuilder sb = new StringBuilder();
            foreach (string part in SplitKeys)
            {
                string[] splitPair = part.Split(stringSeparatorsEq, StringSplitOptions.None);
                if (splitPair.Length == 2)
                {
                    for (int i = 0; i < splitPair[1].Length; i++)
                    {
                        if ((splitPair[1])[i] == '1')
                            sb.Append('1');
                        if ((splitPair[1])[i] == '0')
                            sb.Append('0');
                    }
                    EncodingMap.Add(sb.ToString(), byte.Parse(splitPair[0]));
                    sb.Length = 0;
                }
            }
            //The beginning of the compressed code
            int begin = 16 + (numofbytes * 8);
            List<Byte> outputarraylist = new List<Byte>();
            StringBuilder Code = new StringBuilder();
            List<bool> reversedData = new List<bool>();
            List<bool> subList = new List<bool>();

            for (int i = begin; i < dataInBits.Count; i++)
            {
                if (EncodingMap.ContainsKey(Code.ToString()))
                {
                    outputarraylist.Add(EncodingMap[Code.ToString()]);
                    Code.Length = 0;
                }
                if (dataInBits[i])
                    Code.Append('1');
                if (!dataInBits[i])
                    Code.Append('0');
            }
            byte[] ret = new byte[outputarraylist.Count];
            for(int i = 0; i < outputarraylist.Count; i++)
            {
                ret[i] = outputarraylist[i];
            }
            return ret;
        }
        public static byte ConvertToByte(BitArray bits)
        {
            if (bits.Count != 8)
            {
                throw new ArgumentException("bits");
            }
            byte[] bytes = new byte[1];
            bits.CopyTo(bytes, 0);
            return bytes[0];
        }
    }
}