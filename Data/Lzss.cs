﻿//Nikolay Chernuha ID: 323434803
//Eyal Ilan ID:304929334
//Naor Zino ID: 200804367

namespace Data
{
    using System;
    using System.IO;
    using Functions;
    using System.Collections.Generic;
    //Serializable keyword allows the system to serialize the file for the ourput
    [Serializable]
    public static class Lzss
    {
        public static readonly byte[] SlidingWindow = new byte[Functions.Constants.WindowSize];  // The sliding window that contains the endoded and not encoded bytes
        public static readonly byte[] UncodedLookahead = new byte[Functions.Constants.MaxCoded]; // Characters to be encoded

        public static readonly int[] EncodedTable = new int[Functions.Constants.HashSize]; // List head for each hush key
        public static readonly int[] Next = new int[Functions.Constants.WindowSize]; // Indices of next elements in the hash list
        public static List<byte> ReturnList = new List<byte>(); //The return list of encoded bytes / decoded
        public static int iter = 0;


        public static byte[] Encode(string inputFileNameForEncode)
        {
            {
                byte[] temp = null;
                int iter = 0;
                try
                {
                    temp = File.ReadAllBytes(inputFileNameForEncode); //Reading the file as bytes, if there is some problem, try will prevent from program to crash
                }
                catch { }
                if (temp == null)
                {
                    return null; //returning null oof case of file error
                }
                {
                    var ByteSeg = 0; //each byte to code
                    var Length = 0; // Length of the input
                    for (Length = 0; Length < Functions.Constants.MaxCoded; Length++)
                    {
                        ByteSeg = temp[iter];
                        iter += 1;
                        if (iter == (temp.Length - 1)) //delimeter that stops the for loop and prevents from accessing out of bounds
                        {
                            break;
                        }

                        UncodedLookahead[Length] = (byte)ByteSeg; //adding each drgent to the uncodedLookahead, The lookavead value is in Constants.cs
                    }

                    InitializeSlidingWindow();

                    // 8 code flags and encoded strings
                    var flags = 0;
                    var flagPosition = 1;
                    var encodedData = new byte[16];
                    var nextEncoded = 0; // Encoded data next index

                    var windowHead = 0; // Head of sliding window
                    var uncodedHead = 0; // Head of uncoded lookahead

                    var i = 0;

                    var MatchedData = FindMatch.FindMatching(uncodedHead);

                    // Now encoding the rest of the file
                    while (Length > 0)
                    {
                        // Garbage beyond last data expands match length
                        if (MatchedData.Length > Length)
                        {
                            MatchedData.Length = Length;
                        }

                        // Not long enough match -> write uncoded byte
                        if (MatchedData.Length <= Functions.Constants.MaxUncoded)
                        {
                            MatchedData.Length = 1; // Set to 1 for 1 byte uncoded
                            flags |= flagPosition; // Mark with uncoded byte flag
                            encodedData[nextEncoded] = UncodedLookahead[uncodedHead];
                            nextEncoded++;
                        }
                        else // matched.Length > MAX_UNCODED -> encode as offset and length
                        {
                            encodedData[nextEncoded] = (byte)((MatchedData.Offset & 0x0FFF) >> 4);
                            nextEncoded++;

                            encodedData[nextEncoded] = (byte)(((MatchedData.Offset & 0x000F) << 4) |
                                (MatchedData.Length - (Functions.Constants.MaxUncoded + 1)));
                            nextEncoded++;
                        }

                        // We have 8 code flags -> write out flags and code buffer
                        if (flagPosition == 0x80)
                        {
                            ReturnList.Add((byte)flags);
                            for (i = 0; i < nextEncoded; i++)
                            {
                                ReturnList.Add(encodedData[i]);
                            }

                            // Reset encoded data buffer
                            flags = 0;
                            flagPosition = 1;
                            nextEncoded = 0;
                        }
                        else // We don't have 8 code flags yet -> use next bit for next flag
                        {
                            flagPosition <<= 1;
                        }

                        // Replace the matchData.Length worth of bytes we've matched in the
                        // sliding window with new bytes from the input file
                        i = 0;
                        while (i < MatchedData.Length)
                        {
                            if (iter >= (temp.Length))
                            {
                                break;
                            }
                            ByteSeg = temp[iter];
                            iter += 1;


                            // Add old byte into sliding window and new into lookahead
                            ReplaceByte.ReplaceChar(windowHead, UncodedLookahead[uncodedHead]);
                            UncodedLookahead[uncodedHead] = (byte)ByteSeg;
                            windowHead = (windowHead + 1) % Functions.Constants.WindowSize;
                            uncodedHead = (uncodedHead + 1) % Functions.Constants.MaxCoded;
                            i++;
                        }

                        // Handle case where we reach the end of file before filling lookahead
                        while (i < MatchedData.Length)
                        {
                            ReplaceByte.ReplaceChar(windowHead, UncodedLookahead[uncodedHead]);

                            // Nothing to add to lookahead here
                            windowHead = (windowHead + 1) % Functions.Constants.WindowSize;
                            uncodedHead = (uncodedHead + 1) % Functions.Constants.MaxCoded;
                            Length--;
                            i++;
                        }

                        // Find matched for the rest bytes
                        MatchedData = Functions.FindMatch.FindMatching(uncodedHead);
                    }

                    // Write out any remaining  data
                    if (nextEncoded != 0)
                    {
                        ReturnList.Add((byte)flags);
                        for (i = 0; i < nextEncoded; i++)
                        {
                            ReturnList.Add(encodedData[i]);
                        }
                    }
                }
                //converting the output data to array as anly byte array could be written to the file
                return ReturnList.ToArray();
            }
        }


        public static byte[] Decode(byte[] input)
        {
            ReturnList = new List<byte>();
            InitializeSlidingWindow();

            var flags = 0; // Encoded flag
            var flagsUsed = 7; // Not encoded flag
            var nextByte = 0; // Next byte in SlidingWindow
            var code = new EncodedData();
            int iter = 0;
            while (true)
            {
                flags >>= 1;
                flagsUsed++;

                // Shifted out all the flag bits - read a new flag
                var readByte = 0;
                if (flagsUsed == 8)
                {
                    try
                    {
                        if ((readByte = input[iter++]) == -1)
                        {
                            break;
                        }
                    }
                    catch { break; }

                    flags = readByte & 0xFF;
                    flagsUsed = 0;
                }

                // Uncoded byte
                if ((flags & 1) != 0)
                {
                    try
                    {
                        if ((readByte = input[iter++]) == -1)
                        {
                            break;
                        }
                    }
                    catch { break; }
                    //Adding the sequence to the return list.
                    ReturnList.Add((byte)readByte);
                    SlidingWindow[nextByte] = (byte)readByte;
                    nextByte = (nextByte + 1) % Functions.Constants.WindowSize;
                }
                else
                {
                    try
                    {
                        if ((code.Offset = input[iter++]) == -1)
                        {
                            break;
                        }
                    }
                    catch { break; }

                    try
                    {
                        if ((code.Length = input[iter++]) == -1)
                        {
                            break;
                        }
                    }
                    catch { break; }

                    // Unpack offset and length
                    code.Offset <<= 4;
                    code.Offset |= (code.Length & 0x00F0) >> 4;
                    code.Length = (code.Length & 0x000F) + Functions.Constants.MaxUncoded + 1;

                    // Write out decoded string to file and lookahead
                    for (var i = 0; i < code.Length; i++)
                    {
                        readByte = (int)(SlidingWindow[(code.Offset + i) % Functions.Constants.WindowSize]);
                        ReturnList.Add((byte)readByte);
                        UncodedLookahead[i] = (byte)readByte;
                    }

                    // Write out decoded string to sliding window
                    for (var i = 0; i < code.Length; i++)
                    {
                        SlidingWindow[(nextByte + i) % Constants.WindowSize] = UncodedLookahead[i];
                    }
                    nextByte = (nextByte + code.Length) % Constants.WindowSize;
                }
            }
            //Returning the List, prior converted to the array
            return ReturnList.ToArray();
        }
        private static void InitializeSlidingWindow()
        {
            // Initializing the SlidingWindow with same values which means 
            // there is only 1 hash key for the entier sliding window
            for (var i = 0; i < Functions.Constants.WindowSize; i++)
            {
                SlidingWindow[i] = 0;
                Next[i] = i + 1;
            }

            // There is no next for the last character
            Next[Functions.Constants.WindowSize - 1] = Functions.Constants.NullIndex;

            // The only list now is the list with zeros
            for (var i = 0; i < Functions.Constants.HashSize; i++)
            {
                EncodedTable[i] = Functions.Constants.NullIndex;
            }

            EncodedTable[Functions.GetHashKey.GetKey(0, false)] = 0;
        }
    }
}
