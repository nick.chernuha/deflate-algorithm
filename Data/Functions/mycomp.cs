﻿//Nikolay Chernuha ID: 323434803
//Eyal Ilan ID:304929334
//Naor Zino ID: 20804367

using System;

namespace Data.Functions
{
    [Serializable]
    public class mycomp
    {
        public byte[] _Arr;
        public bool _LZSS, _Huff;
        public string _Ending;
        public mycomp(byte[] arr, bool LZSS, bool Huff,string Ending)
        {
            _Arr = arr;
            _LZSS = LZSS;
            _Huff = Huff;
            _Ending = Ending;
        }
    }
}
