﻿//Nikolay Chernuha ID: 323434803
//Eyal Ilan ID:304929334
//Naor Zino ID: 200804367

namespace Data.Functions
{
    public static class RemoveStr
    {
        //Removing the string and replacing it with a new one
        public static void RemoveString(int BharIndex)
        {
            var NextIndex = Lzss.Next[BharIndex];
            Lzss.Next[BharIndex] = Constants.NullIndex;

            var HashKey = GetHashKey.GetKey(BharIndex, false);

            // We're deleting a list head
            if (Lzss.EncodedTable[HashKey] == BharIndex)
            {
                Lzss.EncodedTable[HashKey] = NextIndex;
                return;
            }

            // Find byte pointing to ours
            var i = Lzss.EncodedTable[HashKey];
            while (Lzss.Next[i] != BharIndex)
            {
                i = Lzss.Next[i];
            }

            Lzss.Next[i] = NextIndex;
        }

    }
}
