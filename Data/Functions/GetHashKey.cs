﻿//Nikolay Chernuha ID: 323434803
//Eyal Ilan ID:304929334
//Naor Zino ID: 20804367


namespace Data.Functions
{
    public class GetHashKey
    {
        //This function gets the hash from to and adds it to the the requested byte
        public static int GetKey(int Offset, bool IsInLookahead)
        {
            var HashKey = 0;

            if (IsInLookahead)
            {
                //Looking in the uncoded sequence
                for (var i = 0; i < Constants.MaxUncoded + 1; i++)
                {
                    HashKey = (HashKey << 5) ^ Lzss.UncodedLookahead[Offset];
                    HashKey %= Constants.HashSize;
                    Offset = (Offset + 1) % Constants.MaxCoded;
                }
            }
            else
            {
                for (var i = 0; i < Constants.MaxUncoded + 1; i++)
                {
                    HashKey = ((HashKey << 5) ^ Lzss.SlidingWindow[Offset]);
                    HashKey %= Constants.HashSize;
                    Offset = (Offset + 1) % Constants.WindowSize;
                }
            }

            return HashKey;
        }

    }
}
