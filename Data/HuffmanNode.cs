﻿using System;

namespace Data
{
    //HuffmanNode class that inplements IComparable to allow 
    //easy building of the pree with PriorityQueue
    public class HuffmanNode : IComparable
    {
        public HuffmanNode left;
        public HuffmanNode right;
        public byte? Letter;
        public int Freq;

        public HuffmanNode(byte? Letter, int Freq)
        {
            this.Freq = Freq;
            this.Letter = Letter;
            left = null;
            right = null;
        }

        //Compare to function
        public int CompareTo(object obj)
        {
            if (Freq < ((HuffmanNode)obj).Freq)
                return -1;
            if (Freq > ((HuffmanNode)obj).Freq)
                return 1;
            return 0;
        }
    }
}