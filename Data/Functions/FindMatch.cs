﻿//Nikolay Chernuha ID: 323434803
//Eyal Ilan ID:304929334
//Naor Zino ID: 200804367

namespace Data.Functions
{
    //Finding matched sequence in the lookup part of the SlidingWindow
    public static class FindMatch
    {
        public static EncodedData FindMatching(int Uncoded)
        {
            var MatchingData = new EncodedData();

            var i = Lzss.EncodedTable[GetHashKey.GetKey(Uncoded, true)];
            var j = 0;

            while (i != Constants.NullIndex)
            {
                // We've matched the first symbol
                if (Lzss.SlidingWindow[i] == Lzss.UncodedLookahead[Uncoded])
                {
                    j = 1;

                    while (Lzss.SlidingWindow[(i + j) % Constants.WindowSize] ==
                        Lzss.UncodedLookahead[(Uncoded + j) % Constants.MaxCoded])
                    {
                        //Prevents from overflow of maxcoded bytes
                        if (j >= Constants.MaxCoded)
                        {
                            break;
                        }

                        j++;
                    }

                    if (j > MatchingData.Length)
                    {
                        MatchingData.Length = j;
                        MatchingData.Offset = i;
                    }
                }
                //preventing overflow at the end
                if (j >= Constants.MaxCoded)
                {
                    MatchingData.Length = Constants.MaxCoded;
                    break;
                }

                i = Lzss.Next[i];
            }

            return MatchingData;
        }

    }
}
