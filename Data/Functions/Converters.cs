﻿//Nikolay Chernuha ID: 323434803
//Eyal Ilan ID:304929334
//Naor Zino ID: 200804367

using System;
using System.Collections;
using System.Text;

namespace Data.Functions
{
    public static class Converters
    {
        //Converts string to binary representation, ASCII pormat
        public static string ToBinaryString(string v)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] buf = encoding.GetBytes(v);

            StringBuilder binaryStringBuilder = new StringBuilder();
            foreach (byte b in buf)
            {
                binaryStringBuilder.Append(Convert.ToString(b, 2));
            }
            return binaryStringBuilder.ToString();
        }
        //Converts BitArray to string -- This part is from StackOverflow
        public static string BitArrayToStr(BitArray BitArr)
        {
            byte[] strArr = new byte[BitArr.Length / 8];

            ASCIIEncoding encoding = new ASCIIEncoding();

            for (int i = 0; i < BitArr.Length / 8; i++)
            {
                for (int index = i * 8, m = 1; index < i * 8 + 8; index++, m *= 2)
                {
                    strArr[i] += BitArr.Get(index) ? (byte)m : (byte)0;
                }
            }

            return encoding.GetString(strArr);
        }

        public static byte ConvertBoolArrayToByte(bool[] source)
        {
            byte result = 0;
            // This assumes the array never contains more than 8 elements!
            int index = 8 - source.Length;

            // Loop through the array
            foreach (bool b in source)
            {
                // if the element is 'true' set the bit at that position
                if (b)
                    result |= (byte)(1 << (7 - index));

                index++;
            }

            return result;
        }

        public static byte ConvertToByte(BitArray bits)
        {
            if (bits.Count != 8)
            {
                throw new ArgumentException("bits");
            }
            byte[] bytes = new byte[1];
            bits.CopyTo(bytes, 0);
            return bytes[0];
        }

    }
}
