﻿//Nikolay Chernuha ID: 323434803
//Eyal Ilan ID:304929334
//Naor Zino ID: 20804367

using System.Windows;
namespace View
{
    /*Simple initialization of the content inside new window, very self explonatary*/
    public partial class FAQWindow : Window
    {
        public FAQWindow()
        {
            InitializeComponent();
            MainBlock.Text = "This mini-project is presented by:\nNikolay Chernuha ID: 323434803\nEyal Ilan ID:304929334\nNaor Zino ID: 20804367\n\nThe project contains a representation of the DEFLATE compressor, which combines LZSS and Haffmann Coding.\nIn this work we have implemented some  of the source codes from the course studies as well as fromthe net.\n\nReferences:\n\nC#:\nhttps://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/\n\nLZSS:\nhttps://en.wikipedia.org/wiki/Lempel%E2%80%93Ziv%E2%80%93Storer%E2%80%93Szymanski\nhttp://michael.dipperstein.com/lzss/ (Special thanks, very good source!)\nhttps://sites.google.com/site/datacompressionguide/lz77\n\nFor more information about the code, see the included document and project source code.\nThank You!\n";
        }
    }
}
