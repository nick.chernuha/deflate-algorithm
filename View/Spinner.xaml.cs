﻿//Nikolay Chernuha ID: 323434803
//Eyal Ilan ID:304929334
//Naor Zino ID: 20804367

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace View
{
    /// <summary>
    /// Interaction logic for Spinner.xaml
    /// </summary>
    public partial class Spinner : UserControl
    {
        private Storyboard _storyboard;

        public Spinner()
        {
            InitializeComponent();

            this.IsVisibleChanged += OnVisibleChanged;
        }

        private void OnVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (IsVisible)
            {
                StartAnimation();
            }
            else
            {
                StopAnimation();
            }
        }

        private void StartAnimation()
        {
            _storyboard = (Storyboard)FindResource("canvasAnimation");
            _storyboard.Begin(canvas, true);
        }

        private void StopAnimation()
        {
            _storyboard.Remove(canvas);
        }
    }
}

