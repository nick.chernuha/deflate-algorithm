﻿//Nikolay Chernuha ID: 323434803
//Eyal Ilan ID:304929334
//Naor Zino ID: 200804367

namespace Data
{
    public struct EncodedData
    {
        // Offset to start of longest match
        public int Offset { get; set; }

        // Length of longest match
        public int Length { get; set; }
    }
}
